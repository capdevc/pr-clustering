#ifndef _KMEANS_H_
#define _KMEANS_H_
#include "Matrix.h"

Matrix kmeans(const int k, const Matrix& samples, int& itr_ctr, const bool online = false);
Matrix gen_grid(int n, double spacing);
Matrix kohonen(Matrix &map, Matrix &samples, double sigma, double epsilon, double epsilon_decay, int &itr_ctr);

#endif /* _KMEANS_H_ */
