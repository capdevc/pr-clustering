#include <random>
#include <vector>
#include <set>
#include <algorithm>
#include <limits>
#include <cmath>
#include <ctime>
#include "KMeans.hpp"

void kmeans_init(Matrix& centroids, const Matrix& samples, std::vector<int> &cluster) {
    const int d = samples.getCol();              // features
    const int m = samples.getRow();              // samples
    const int k = centroids.getRow();            // # of centroids
    std::vector<pair<int, double>> indices(m);   // indices of available points and closest centroid

    // initialize the random number generator, g
    std::default_random_engine g;

    // initialize the indices
    for (int i = 0; i < m; ++i) {
        indices[i] = std::make_pair(i, numeric_limits<double>::max());
    }

    // pick the first sample
    std::uniform_int_distribution<int> uni(0, samples.getRow() - 1);
    int samp = uni(g);
    for (int i = 0; i < d; ++i) {
        centroids(0,i) = samples(samp,i);
    }
    // take it out of the available list
    cluster[indices[samp].first] = 0;
    indices[samp] = indices.back();
    indices.pop_back();

    // the k-means++ algorithm
    for (int s = 0; s < k-1; ++s) {
        // calculate distances to closest centroid
		for (auto &i : indices) {
			double dist = 0;
			for (int j = 0; j < d; ++j) {
				dist += std::pow(centroids(s, j) - samples(i.first, j), 2);
			}
			if (dist < i.second) {
				i.second = dist;
			}
		}
        // select a point from the weighted distribution
        // std::discrete_distribution<int> disc(begin(weights[s]), end(weights[s]));
		std::discrete_distribution<> disc(indices.size(), 0, indices.size(), [&](double x)->double {
			return indices[static_cast<int>(x)].second;
		});
        samp = disc(g);
        for (int i = 0; i < d; ++i) {
            centroids(s+1,i) = samples(indices[samp].first, i);
        }
        cluster[indices[samp].first] = s + 1;
        // make that point unavailable
        indices[samp] = indices.back();
        indices.pop_back();
    }
}


Matrix kmeans(const int k, const Matrix& samples, int& itr_ctr, const bool online) {
    const int d = samples.getCol();               // features
    const int m = samples.getRow();               // samples
    Matrix centroids;                             // centroid matrix
    std::vector<int> cluster(m, k);               // maps samples to centroids
    std::set<int> updated;                        // cluster that have changed
    std::vector<std::set<int>> members(k + 1);    // maps centroids to sample vectors

    // usr k-means++ to initialize
    // http://en.wikipedia.org/wiki/K-means++
    centroids.createMatrix(k, d);
    kmeans_init(centroids, samples, cluster);

	// set initial membership
	for (int i = 0; i < m; ++i) {
		members[cluster[i]].insert(i);
	}

    // iterate until we have no updates
    updated.insert(0);
    while (!updated.empty()) {
        ++itr_ctr;
        updated.clear();
        for (int i = 0; i < m; ++i) {
            // recalculate class membership for sample i
            double closest_dist = numeric_limits<double>::max();
            int winner = cluster[i];
            for (int j = 0; j < k; ++j) {
                double dist = 0;
                for (int x = 0; x < d; ++x) {
                    dist += std::pow(centroids(j, x) - samples(i, x), 2);
                }
                if (dist < closest_dist) {
                    closest_dist = dist;
                    winner = j;
                }
            }
			// if we have a new winner, flag the two clusters for update
            if (cluster[i] != winner) {
                updated.insert(cluster[i]);
                updated.insert(winner);
                members[winner].insert(i);
                members[cluster[i]].erase(i);
                cluster[i] = winner;
            }
            // update centroids if we're learning online
            if (online && !updated.empty()) {
				updated.erase(k);
				for (int x = 0; x < d; ++x) {
					for (auto c : updated) {
						centroids(c, x) = 0;
						for (auto s : members[c]) {
							centroids(c, x) += samples(s, x);
						}
						centroids(c, x) = std::round(centroids(c, x) / members[c].size());
					}
				}
				updated.clear();
				updated.insert(k);
            }
        }
        // update centroids
        if (!online) {
			updated.erase(k);
			for (int x = 0; x < d; ++x) {
				for (auto c : updated) {
					centroids(c, x) = 0;
					for (auto s : members[c]) {
						centroids(c, x) += samples(s, x);
					}
					centroids(c, x) = std::round(centroids(c, x) / members[c].size());
				}
			}
        }
		cout << itr_ctr << " ";
        cout.flush();
    }
	cout << endl;
    return centroids;
}


Matrix gen_grid(int n, double spacing) {
	int k = static_cast<int>(std::pow(n, 2));
	Matrix map(k, k);
	auto dist = [=](int a, int b) -> double {
		return std::pow(spacing * ((a % k) - (b % k)), 2) + std::pow(spacing * ((a / k) - (b / k)), 2);
	};
	for (int i = 0; i < k; ++i) {
		for (int j = 0; j < k; ++j) {
			map(i, j) = dist(i, j);
		}
	}
	return map;
}

// take a k x k square matrix of weights and a set of samples
// and apply SOM clustering, returning centroids.
Matrix kohonen(Matrix &map, Matrix &samples, double sigma, double epsilon, double epsilon_decay, int &itr_ctr) {
	int k = map.getCol();						// number of clusters
	int m = samples.getRow();					// number of samples
	int d = samples.getCol();					// number of features
	Matrix centroids(k, d);						// Matrix of centroids
	std::vector<int> cluster(m, k);				// maps sample to centroid
	double twosigsq = 2 * std::pow(sigma, 2);
	
	// initialize the centroids
	std::vector<int> rsamp(m);
	int count = 0;
	std::for_each(begin(rsamp), end(rsamp), [&](int &x) {x = count++; });
	std::srand(std::time(0));
	std::random_shuffle(begin(rsamp), end(rsamp));
	for (int ki = 0; ki < k; ++ki) {
		for (int di = 0; di < d; ++di) {
			centroids(ki, di) = samples(rsamp[ki], di);
		}
	}

	bool updated = true;
	while (updated) {
		itr_ctr++;
		updated = false;
		for (int mi = 0; mi < m; ++mi) {
			int winner;
			double closest_dist = std::numeric_limits<double>::max();
			for (int ki = 0; ki < k; ++ki) {
				double dist = 0;
				for (int di = 0; di < d; ++di) {
					dist += std::pow(centroids(ki, di) - samples(mi, di), 2);
				}
				if (dist < closest_dist) {
					closest_dist = dist;
					winner = ki;
				}
			}
			if (cluster[mi] != winner) {
				updated = true;
				cluster[mi] = winner;
				for (int ki = 0; ki < k; ++ki) {
					double delta = epsilon * std::exp(-map(winner, ki) / twosigsq);
					for (int di = 0; di < d; ++di) {
						centroids(ki, di) += delta * (samples(mi, di) - centroids(ki, di));
					}
				}
			}
		}
		epsilon = epsilon_decay * epsilon;
		cout << itr_ctr << " ";
        cout.flush();
	}
	cout << endl;
	return centroids;
}