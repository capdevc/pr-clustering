#include <iostream>
#include <cmath>
#include <string>
#include "Pr.h"
#include "Matrix.h"
#include "KMeans.hpp"

double recolor(Matrix &img, Matrix &centroids, Matrix &diff) {
    int m = img.getRow();
    int k = centroids.getRow();
    double error = 0;
    double e = 0;

    for (int i = 0; i < m; ++i) {
        int winner = k;
        double dist;
        double closest_dist = std::numeric_limits<double>::max();
        for (int j = 0; j < k; ++j) {
            dist = 0;
            for (int x = 0; x < 3; ++x) {
                dist += std::pow(img(i, x) - centroids(j, x), 2);
            }
            if (dist < closest_dist) {
                closest_dist = dist;
                winner = j;
            }
        }
        for (int x = 0; x < 3; ++x) {
            e = img(i, x) - centroids(winner, x);
            diff(i, x) = std::abs(e * 4);
            error += std::pow(e, 2);
            img(i, x) = centroids(winner, x);
        }
    }
    return std::sqrt(error/img.getRow());
}


void usage(char *argv[]) {
    std::cout << "Usage: " << argv[0] << " <input image> <output image> <x> <y> [k|w|s] <options>" << std::endl;
    std::cout << "   input image       : ppm file to be recolored" << std::endl;
    std::cout << "   output image      : file name for recolored image" << std::endl;
    std::cout << "   x                 : x dimension of image" << std::endl;
    std::cout << "   y                 : y dimension of image" << std::endl;
    std::cout << "   k <# clusters>    : normal k means" << std::endl;
    std::cout << "   w <# of clusters> : number of clusters to use" << std::endl;
    std::cout << "   s <n> <s> <e> <d> : som/kohonen map" << std::endl;
    std::cout << "                       n = side length of grid topology" << std::endl;
    std::cout << "                       s = sigma" << std::endl;
    std::cout << "                       e = epsilon" << std::endl;
    std::cout << "                       d = decay rate per iteration s.t. e =  d * e and s = d * s" << std::endl;
}

int main(int argc, char* argv[]) {
    Matrix centroids;               // Matrix of cluster centroids
    Matrix map;                     // Matrix of distance between centroids for kohonen
    string command;                 // subroutine selector
    bool wta;                       // winner takes all?
    int k;                          // number of clusters for kmeans
    int x;                          // image size x
    int y;                          // image size y
    int n;                          // kohonen map side length. k = n^2
    int itr_ctr = 0;                // iteration counter
    double s;                       // sigma
    double e;                       // epsilon
    double d;                       // decay rate: sigma(t+1) = d * sigma(t)

    if (argc < 7) {
        usage(argv);
        exit(EXIT_FAILURE);
    }
    command = std::string(argv[5]);
    x = std::stoi(std::string(argv[3]));
    y = std::stoi(std::string(argv[4]));
    // read in the input file;
    Matrix image = readImage(argv[1], &x, &y);
    // parse and execute the command
    if (command == "k" || command == "w") {
        if (argc != 7) {
            usage(argv);
            exit(EXIT_FAILURE);
        }
        k = std::stoi(std::string(argv[6]));
        if (command == "w") {
            wta = true;
        }
        else {
            wta = false;
        }
        centroids = kmeans(k, image, itr_ctr, wta);
    } else if (command == "s") {
        if (argc != 10) {
            usage(argv);
            exit(EXIT_FAILURE);
        }
        n = std::stoi(std::string(argv[6]));
        s = std::stod(std::string(argv[7]));
        e = std::stod(std::string(argv[8]));
        d = std::stod(std::string(argv[9]));
        map = gen_grid(n, 1);
        centroids = kohonen(map, image, s, e, d, itr_ctr);
    } else {
        usage(argv);
        exit(EXIT_FAILURE);
    }

    std::cout << "Number of iterations: " << itr_ctr << std::endl;
    // recolor the image and write out the diff.
    Matrix new_image = image;
    Matrix diff = image;
    double mse = recolor(new_image, centroids, diff);
    cout << "MSE: " << mse << endl;
    writeImage(argv[2], new_image, x, y);
    std::string diffname = std::string(argv[2]);
    diffname.insert(diffname.size() - 4, "-diff");
    writeImage(diffname.c_str(), diff, x, y); 
    return 0;
}
